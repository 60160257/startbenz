/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author acer
 */
public class ReceiptDetailModel {

    private int ReceiptProductID;
    private String product;
    private int receiptID;
    private int ReceiptProductUnit;
    private double price;
    private double total;

    public ReceiptDetailModel(int ReceiptProductID, String product, int receiptID, int ReceiptProductUnit, double price, double total) {
        this.ReceiptProductID = ReceiptProductID;
        this.product = product;
        this.receiptID = receiptID;
        this.ReceiptProductUnit = ReceiptProductUnit;
        this.price = price;
        this.total = total;
    }

    public int getReceiptProductID() {
        return ReceiptProductID;
    }

    public void setReceiptProductID(int ReceiptProductID) {
        this.ReceiptProductID = ReceiptProductID;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public int getReceiptID() {
        return receiptID;
    }

    public void setReceiptID(int receiptID) {
        this.receiptID = receiptID;
    }

    public int getReceiptProductUnit() {
        return ReceiptProductUnit;
    }

    public void setReceiptProductUnit(int ReceiptProductUnit) {
        this.ReceiptProductUnit = ReceiptProductUnit;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "ReceiptDetailModel{" + "ReceiptProductID=" + ReceiptProductID + ", product=" + product + ", receiptID=" + receiptID + ", ReceiptProductUnit=" + ReceiptProductUnit + ", price=" + price + ", total=" + total + '}';
    }
    
    

    
}
