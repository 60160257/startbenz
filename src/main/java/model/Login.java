/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.text.SimpleDateFormat;

/**
 *
 * @author user
 */
public class Login {
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    private int id;
    private int empid;
    private String timeIn;
    private String timeOut;
    
    public Login(int id,int empid,String timeIn,String timeOut){
        this.id = id;
        this.empid = empid;
        this.timeIn = simpleDateFormat.format(new java.util.Date());;
        this.timeOut = simpleDateFormat.format(new java.util.Date());;
    }
    public Login(int empid,String timeIn){
        this.id = id;
        this.empid = empid;
        this.timeIn = simpleDateFormat.format(new java.util.Date());;
        this.timeOut = simpleDateFormat.format(new java.util.Date());;
    }
    
    public int getId(){
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEmpid() {
        return empid;
    }

    public void setEmpid(int empid) {
        this.empid = empid;
    }

    public String getTimeIn() {
        return timeIn;
    }

    public void setTimeIn(String timeIn) {
        this.timeIn = timeIn;
    }

    public String getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(String timeOut) {
        this.timeOut = timeOut;
    }

    @Override
    public String toString() {
        return "ID: "+id + "TimeIn: "+timeIn +"EmpID: "+empid +"TimeOut"+timeOut; //To change body of generated methods, choose Tools | Templates.
    }
}
