/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author acer
 */
public class Product {
   private int ProductID;
   private String ProductName;
   private Double ProductPrice;
   private int ProductAmount;
   private String ProductImg;
   private String ProductType;

    public Product(int ProductID, String ProductName, Double ProductPrice, int ProductAmount, String ProductImg, String ProductType) {
        this.ProductID = ProductID;
        this.ProductName = ProductName;
        this.ProductPrice = ProductPrice;
        this.ProductAmount = ProductAmount;
        this.ProductImg = ProductImg;
        this.ProductType = ProductType;
    }

    public int getProductID() {
        return ProductID;
    }

    public void setProductID(int ProductID) {
        this.ProductID = ProductID;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String ProductName) {
        this.ProductName = ProductName;
    }

    public Double getProductPrice() {
        return ProductPrice;
    }

    public void setProductPrice(Double ProductPrice) {
        this.ProductPrice = ProductPrice;
    }

    public int getProductAmount() {
        return ProductAmount;
    }

    public void setProductAmount(int ProductAmount) {
        this.ProductAmount = ProductAmount;
    }

    public String getProductImg() {
        return ProductImg;
    }

    public void setProductImg(String ProductImg) {
        this.ProductImg = ProductImg;
    }

    public String getProductType() {
        return ProductType;
    }

    public void setProductType(String ProductType) {
        this.ProductType = ProductType;
    }

    @Override
    public String toString() {
        return "Product{" + "ProductID=" + ProductID + ", ProductName=" + ProductName + ", ProductPrice=" + ProductPrice + ", ProductAmount=" + ProductAmount + ", ProductImg=" + ProductImg + ", ProductType=" + ProductType + '}';
    }
   
}
