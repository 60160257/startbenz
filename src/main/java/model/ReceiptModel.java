/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.beans.Customizer;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.ArrayList;

/**
 *
 * @author acer
 */
public class ReceiptModel {

    private int ReceiptID,ReceiptAmount;
    private int customerId ; 
    private int employeeId;
    private String ReceiptDate;
    private Double ReceiptTotal;
    private ArrayList<ReceiptDetailModel> receiptDetail;

    public ReceiptModel(int ReceiptID, String ReceiptDate, int customer, int employee, int ReceiptAmount, Double ReceiptTotal) {
        this.ReceiptID = ReceiptID;
        this.ReceiptAmount = ReceiptAmount;
        this.customerId = customer;
        this.employeeId = employee;
        this.ReceiptDate = ReceiptDate;
        this.ReceiptTotal = ReceiptTotal;
        receiptDetail = new ArrayList<>();
    }

    public ReceiptModel(LocalDateTime now, String text, int eId, int rowCount, String text0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getReceiptID() {
        return ReceiptID;
    }

    public void setReceiptID(int ReceiptID) {
        this.ReceiptID = ReceiptID;
    }

    public int getReceiptAmount() {
        return ReceiptAmount;
    }

    public void setReceiptAmount(int ReceiptAmount) {
        this.ReceiptAmount = ReceiptAmount;
    }

    public int getCustomer() {
        return customerId;
    }

    public void setCustomer(int customer) {
        this.customerId = customer;
    }

    public int getEmployee() {
        return employeeId;
    }

    public void setEmployee(int employee) {
        this.employeeId = employee;
    }

    public String getReceiptDate() {
        return ReceiptDate;
    }

    public void setReceiptDate(String ReceiptDate) {
        this.ReceiptDate = ReceiptDate;
    }

    public Double getReceiptTotal() {
        return ReceiptTotal;
    }

    public void setReceiptTotal(Double ReceiptTotal) {
        this.ReceiptTotal = ReceiptTotal;
    }

    @Override
    public String toString() {
        return "Receipt{" + "ReceiptID=" + ReceiptID + ", ReceiptAmount=" + ReceiptAmount + ", customer=" + customerId + ", employee=" + employeeId + ", ReceiptDate=" + ReceiptDate + ", ReceiptTotal=" + ReceiptTotal + '}';
    }
   

   
    
}
