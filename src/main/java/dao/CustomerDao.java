/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;


import database.Database;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.Employee;

/**
 *
 * @author acer
 */
public class CustomerDao implements DaoInterface<Customer> {

    @Override
    public int add(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
         int id = -1;
        try {
            String sql="INSERT INTO Customer(CustomerFirstName, CustomerLastName,"
                    + "CustomerPhone, CustomerApplyDate, CustomerBalance)VALUES(?,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,object.getFirstName());
            stmt.setString(2, object.getLastName());
            stmt.setString(3, object.getTel());
            stmt.setString(4, object.getApplyDate());
            stmt.setDouble(5, object.getBalance());
            int row =stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();           
            if(result.next()){
                id= result.getInt(1);
            }

        } catch (SQLException ex) {
            System.out.println(ex);
        }

        db.close();
        return id;
    }

    @Override
    public ArrayList<Customer> getAll() {
        ArrayList<Customer> list = new ArrayList();
         Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT  CustomerID, CustomerFirstName, CustomerLastName,"
                    + "CustomerPhone, CustomerApplyDate, CustomerBalance FROM Customer";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("CustomerID");
                String fname = result.getString("CustomerFirstName");
                String lname = result.getString("CustomerLastName");
                String tel = result.getString("CustomerPhone");
                String applyDate = result.getString("CustomerApplyDate");
                Double balance = result.getDouble("CustomerBalance");
                
                Customer customer = new Customer(id, fname, lname, tel, applyDate, balance);
                list.add(customer);

            }

        } catch (SQLException ex) {
            System.out.println(ex);
        }

        db.close();
        return list;
    }
    
    public ArrayList<Customer> getPhone(String p) {
        ArrayList<Customer> list = new ArrayList();
         Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT  CustomerID, CustomerFirstName, CustomerLastName,"
                    + "CustomerPhone, CustomerApplyDate, CustomerBalance FROM Customer WHERE CustomerPhone=\""+p+"\"";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("CustomerID");
                String fname = result.getString("CustomerFirstName");
                String lname = result.getString("CustomerLastName");
                String tel = result.getString("CustomerPhone");
                String applyDate = result.getString("CustomerApplyDate");
                Double balance = result.getDouble("CustomerBalance");
                
                Customer customer = new Customer(id, fname, lname, tel, applyDate, balance);
                list.add(customer);

            }

        } catch (SQLException ex) {
            System.out.println(ex);
        }

        db.close();
        return list;
    }

    @Override
    public Customer get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT  CustomerID, CustomerFirstName, CustomerLastName,"
                    + "CustomerPhone, CustomerApplyDate, CustomerBalance FROM Customer WHERE CustomerID="+id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if(result.next()) {
                int cid = result.getInt("CustomerID");
                String fname = result.getString("CustomerFirstName");
                String lname = result.getString("CustomerLastName");
                String tel = result.getString("CustomerPhone");
                String applyDate = result.getString("CustomerApplyDate");
                Double balance = result.getDouble("CustomerBalance");
                Customer customer = new Customer(cid, fname, lname, tel, applyDate, balance);
                return customer;

            }

        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row =0;
        try {
            String sql = "DELETE FROM Customer WHERE CustomerID=?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(Customer object) {
        Connection conn = null;
        String dbPath = "./db/startbenz.db";
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE Customer SET CustomerFirstName=?, CustomerLastName=?,"
                    + "CustomerPhone=?, CustomerApplyDate=?, CustomerBalance=? WHERE CustomerID=? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getFirstName());
            stmt.setString(2, object.getLastName());
            stmt.setString(3, object.getTel());
            stmt.setString(4, object.getApplyDate());
            stmt.setDouble(5, object.getBalance());
            stmt.setInt(6, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
           System.out.println(ex);
        }

        db.close();
        return row;
    }
    
}
