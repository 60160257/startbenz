/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Login;

/**
 *
 * @author user
 */
public class LoginDao implements DaoInterface<Login>{
    
    @Override
    public int add(Login object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Login(LoginDate,"
                    + "EmployeeID,"
                    + ")VALUES(?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getTimeIn());
            stmt.setInt(2, object.getEmpid());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }

        } catch (SQLException ex) {
            System.out.println(ex);
        }

        db.close();
        return id;
    }

    @Override
    public ArrayList<Login> getAll() {
       ArrayList<Login> list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
           String sql = "SELECT LoginID,\n" +
                        "       LoginDate,\n" +
                        "       EmployeeID,\n" +
                        "       LogoutDate,\n" +
                        "  FROM Login\n" ;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()) {
                int id = result.getInt("LoginID");
                int empId = result.getInt("EmplyeeID");
                String timeIn = result.getString("LoginDate");
                String timeOut = result.getString("LogoutDate");

               Login login = new Login(id, empId, timeIn, timeOut);
                System.out.println(login);
                list.add(login);
            }
        } catch (SQLException ex) {
            //Logger.getLogger(TestSelectTimeStamp.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }
    
    public Login get(int id){
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
           String sql = "SELECT LoginID,\n" +
                        "       LoginDate,\n" +
                        "       EmployeeID,\n" +
                        "       LogoutDate,\n" +
                        "  FROM Login\n" ;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()) {
                int tid = result.getInt("LoginID");
                int empId = result.getInt("EmplyeeID");
                String timeIn = result.getString("LoginDate");
                String timeOut = result.getString("LogoutDate");

               Login login = new Login(id, empId, timeIn, timeOut);
                System.out.println(login);
            }
        } catch (SQLException ex) {
            //Logger.getLogger(TestSelectTimeStamp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    @Override
    public int delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int update(Login object) {
       Connection conn = null;
        String dbPath = "./db/startbenz.db";
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE Login SET LogoutDate=? WHERE LoginID=? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getTimeOut());
            stmt.setInt(2, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
           System.out.println(ex);
        }

        db.close();
        return row;
    }

    public void add(int empId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
