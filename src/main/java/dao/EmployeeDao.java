/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;
import model.Employee;

/**
 *
 * @author acer
 */
public class EmployeeDao implements DaoInterface<Employee> {

    @Override
    public int add(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Employee(EmployeeFirstName,"
                    + "EmployeeLastName,"
                    + "EmployeePhone,"
                    + "EmployeeSalary,"
                    + "EmployeePassword,"
                    + "Position" + ")VALUES(?,?,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getFirstName());
            stmt.setString(2, object.getLastName());
            stmt.setString(3, object.getTel());
            stmt.setDouble(4, object.getSalary());
            stmt.setString(5, object.getPassword());
            stmt.setString(6, object.getPosition());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }

        } catch (SQLException ex) {
            System.out.println(ex);
        }

        db.close();
        return id;
    }

    @Override
    public ArrayList<Employee> getAll() {
        ArrayList<Employee> list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT EmployeeID,"
                    + "EmployeeFirstName,"
                    + "EmployeeLastName,"
                    + "EmployeePhone,"
                    + "EmployeeSalary,"
                    + "EmployeePassword,"
                    + "Position FROM Employee";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("EmployeeID");
                String fname = result.getString("EmployeeFirstName");
                String lname = result.getString("EmployeeLastName");
                String phone = result.getString("EmployeePhone");
                Double salary = result.getDouble("EmployeeSalary");
                String password = result.getString("EmployeePassword");
                String position = result.getString("Position");
                Employee user = new Employee(id, fname, lname,
                        phone, salary, password, position);
                list.add(user);

            }

        } catch (SQLException ex) {
            System.out.println(ex);
        }

        db.close();
        return list;
    }

    @Override
    public Employee get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT EmployeeID,"
                    + "EmployeeFirstName,"
                    + "EmployeeLastName,"
                    + "EmployeePhone,"
                    + "EmployeeSalary,"
                    + "EmployeePassword,"
                    + "Position FROM Employee WHERE EmployeeID=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int eid = result.getInt("EmployeeID");
                String fname = result.getString("EmployeeFirstName");
                String lname = result.getString("EmployeeLastName");
                String phone = result.getString("EmployeePhone");
                Double salary = result.getDouble("EmployeeSalary");
                String password = result.getString("EmployeePassword");
                String position = result.getString("Position");
                Employee user = new Employee(eid, fname, lname,
                        phone, salary, password, position);
                return user;

            }

        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return null;
    }
    

    public ArrayList<Employee> getPhone(String p) {
        System.out.println(p);
        ArrayList<Employee> list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT EmployeeID,"
                    + "EmployeeFirstName,"
                    + "EmployeeLastName,"
                    + "EmployeePhone,"
                    + "EmployeeSalary,"
                    + "EmployeePassword,"
                    + "Position FROM Employee WHERE EmployeeFirstName=\""+p+"\"";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("EmployeeID");
                String fname = result.getString("EmployeeFirstName");
                String lname = result.getString("EmployeeLastName");
                String phone = result.getString("EmployeePhone");
                Double salary = result.getDouble("EmployeeSalary");
                String password = result.getString("EmployeePassword");
                String position = result.getString("Position");
                Employee user = new Employee(id, fname, lname, phone, salary, password, position);
                list.add(user);

            }

        } catch (SQLException ex) {
            System.out.println(ex);
        }

        db.close();
        return list;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM Employee WHERE EmployeeID=? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(Employee oblect) {
        Connection conn = null;
        String dbPath = "./db/startbenz.db";
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE Employee SET EmployeeFirstName =?,"
                    + "EmployeeLastName=?,"
                    + "EmployeePhone=?,"
                    + "EmployeeSalary=?,"
                    + "EmployeePassword=?,"
                    + "Position=? WHERE EmployeeID=? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, oblect.getFirstName());
            stmt.setString(2, oblect.getLastName());
            stmt.setString(3, oblect.getTel());
            stmt.setDouble(4, oblect.getSalary());
            stmt.setString(5, oblect.getPassword());
            stmt.setString(6, oblect.getPosition());
            stmt.setInt(7, oblect.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex);
        }

        db.close();
        return row;
    }

}
