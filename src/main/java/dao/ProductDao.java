/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import model.Product;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class ProductDao implements DaoInterface<Product> {

    @Override
    public int add(Product object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Product(ProductName,ProductPrice,ProductAmount,ProductImg,ProductType)VALUES(?,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getProductName());
            stmt.setDouble(2, object.getProductPrice());
            stmt.setInt(3, object.getProductAmount());
            stmt.setString(4, object.getProductImg());
            stmt.setString(5, object.getProductType());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }

        } catch (SQLException ex) {
            System.out.println(ex);
        }

        db.close();
        return id;
    }

    @Override
    public ArrayList<Product> getAll() {
        ArrayList<Product> list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT ProductID,ProductName,ProductPrice,ProductAmount,ProductImg, ProductType FROM Product";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("ProductID");
                String name = result.getString("ProductName");
                double price = result.getDouble("ProductPrice");
                int amount = result.getInt("ProductAmount");
                String img = result.getString("ProductImg");
                String type = result.getString("ProductType");
                Product product = new Product(id, name, price, amount, img, type);
                list.add(product);

            }

        } catch (SQLException ex) {
            System.out.println(ex);
        }

        db.close();
        return list;
    }
    
    public ArrayList<Product> getID(String p) {
        ArrayList<Product> list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT ProductID,ProductName,ProductPrice,ProductAmount,ProductImg, ProductType FROM Product WHERE ProductID="+p;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("ProductID");
                String name = result.getString("ProductName");
                double price = result.getDouble("ProductPrice");
                int amount = result.getInt("ProductAmount");
                String img = result.getString("ProductImg");
                String type = result.getString("ProductType");
                Product product = new Product(id, name, price, amount, img, type);
                list.add(product);

            }

        } catch (SQLException ex) {
            System.out.println(ex);
        }

        db.close();
        return list;
    }

    @Override
    public Product get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT ProductID,ProductName,ProductPrice,ProductAmount,ProductImg, ProductType FROM Product WHERE ProductID=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("ProductID");
                String name = result.getString("ProductName");
                double price = result.getDouble("ProductPrice");
                int amount = result.getInt("ProductAmount");
                String img = result.getString("ProductImg");
                String type = result.getString("ProductType");
                Product product = new Product(pid, name, price, amount, img, type);
                return product;

            }

        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM Product WHERE ProductID=? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(Product object) {
        Connection conn = null;
        String dbPath = "./db/startbanz.db";
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE Product SET ProductName=?,ProductPrice=?,ProductAmount=?,ProductImg=?, ProductType=? WHERE ProductID=? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getProductName());
            stmt.setDouble(2, object.getProductPrice());
            stmt.setInt(3, object.getProductAmount());
            stmt.setString(4, object.getProductImg());
            stmt.setString(5, object.getProductType());
            stmt.setInt(6, object.getProductID());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex);
        }

        db.close();
        return row;
    }

}
