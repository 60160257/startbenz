/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import model.ReceiptDetailModel;

/**
 *
 * @author arpao
 */
public class ReceiptDetailDao implements DaoInterface<ReceiptDetailModel> {

    @Override
    public int add(ReceiptDetailModel oblect) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public ArrayList<ReceiptDetailModel> getAll(int id) {
        ArrayList<ReceiptDetailModel> list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT ReceiptProductID, ProductName, ReceiptID, ReceiptProductUnits, ProductPrice, ProductPrice*ReceiptProductUnits FROM ReceiptDetail INNER JOIN Product ON ReceiptDetail.ProductID=Product.ProductID WHERE ReceiptDetail.ReceiptID="+id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int receiptProductID = result.getInt("ReceiptProductID");
                String productName = result.getString("ProductName");
                int receiptID = result.getInt("ReceiptID");
                int receiptProductUnits = result.getInt("ReceiptProductUnits");
                double productPrice = result.getDouble("ProductPrice");
                double total = result.getDouble("ProductPrice*ReceiptProductUnits");
                ReceiptDetailModel receipt = new ReceiptDetailModel(receiptProductID, productName, receiptID, receiptProductUnits, productPrice, total);
                list.add(receipt);

            }

        } catch (SQLException ex) {
            System.out.println(ex);
        }

        db.close();
        return list;
    }

    @Override
    public ReceiptDetailModel get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT ReceiptProductID, ProductName, ReceiptID, ReceiptProductUnits, ProductPrice, ProductPrice*ReceiptProductUnits FROM ReceiptDetail INNER JOIN Product ON ReceiptDetail.ProductID=Product.ProductID WHERE ReceiptID=;"+id;
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                int receiptProductID = result.getInt("ReceiptProductID");
                String productName = result.getString("ProductName");
                int receiptID = result.getInt("ReceiptID");
                int receiptProductUnits = result.getInt("ReceiptProductUnits");
                double productPrice = result.getDouble("ProductPrice");
                double total = result.getDouble("ProductPrice*ReceiptProductUnits");
                ReceiptDetailModel receipt = new ReceiptDetailModel(receiptProductID, productName, receiptID, receiptProductUnits, productPrice, total);

                return receipt;

            }

        } catch (SQLException ex) {
            System.out.println("Error: Unable to select receipt id" + id + "!!!" + ex.getMessage());
        }
        return null;
    }

    @Override
    public int delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int update(ReceiptDetailModel oblect) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<ReceiptDetailModel> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
