/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import model.ReceiptModel;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.Product;
import model.ReceiptDetailModel;
import model.Employee;

/**
 *
 * @author acer
 */
public class ReceiptDao implements DaoInterface<ReceiptModel> {

    @Override
    public int add(ReceiptModel object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Receipt(ReceiptDate,CustomerID,EmployeeID,ReceiptAmount,ReceiptTotal)VALUES(?,?,?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getReceiptDate());
            stmt.setInt(2, object.getCustomer());
            stmt.setInt(3, object.getEmployee());
            stmt.setInt(4, object.getReceiptAmount());
            stmt.setDouble(5, object.getReceiptTotal());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }

        } catch (SQLException ex) {
            System.out.println("Error: to create receipt" + ex.getMessage());
        }

        db.close();
        return id;
    }

    @Override
    public ArrayList<ReceiptModel> getAll() {
        ArrayList<ReceiptModel> list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT ReceiptID,ReceiptDate,CustomerID,EmployeeID,ReceiptAmount,ReceiptTotal FROM Receipt;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("ReceiptID");
                String date = result.getString("ReceiptDate");
                int customerId = result.getInt("CustomerID");
                int employeeId = result.getInt("EmployeeID");
                int amount = result.getInt("ReceiptAmount");
                Double total = result.getDouble("ReceiptTotal");
                ReceiptModel receipt = new ReceiptModel(id, date, customerId, employeeId, amount, total);
                list.add(receipt);

            }

        } catch (SQLException ex) {
            System.out.println("Error: Unable to select all receipt!!!" + ex.getMessage());
        }

        db.close();
        return list;
    }

    public ArrayList<ReceiptModel> getID(String p) {
        ArrayList<ReceiptModel> list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT ReceiptID,ReceiptDate,CustomerID,EmployeeID,ReceiptAmount,ReceiptTotal FROM Receipt WHERE ReceiptID=" + p;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("ReceiptID");
                String date = result.getString("ReceiptDate");
                int customerId = result.getInt("CustomerID");
                int employeeId = result.getInt("EmployeeID");
                int amount = result.getInt("ReceiptAmount");
                Double total = result.getDouble("ReceiptTotal");
                ReceiptModel receipt = new ReceiptModel(id, date, customerId, employeeId, amount, total);
                list.add(receipt);

            }

        } catch (SQLException ex) {
            System.out.println("Error: Unable to select all receipt!!!" + ex.getMessage());
        }

        db.close();
        return list;
    }

    @Override
    public ReceiptModel get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT r.ReceiptID,\n"
                    + "       r.ReceiptID,\n"
                    + "       c.CustomerID,\n"
                    + "       c.CustomerName,\n"
                    + "       c.CustomerPhone,\n"
                    + "       e.EmployeeID,\n"
                    + "       e.EmployeeFirstName,\n"
                    + "       e.EmployeePhone,\n"
                    + "       r.ReceiptTotal\n"
                    + "  FROM Receipt r, Customer c, Employee e\n"
                    + "  WHERE r.ReceiptID=? AND r.CustomerID = c.CustomerID AND r.EmployeeID = e.EmployeeID "
                    + "  ORDER BY ReceiptDate DESC;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                int rid = result.getInt("ReceiptID");
                String created = result.getString("ReceiptDate");
                int customerId = result.getInt("CustomerID");
                int employeeId = result.getInt("EmployeeID");
                int amount = result.getInt("ReceiptAmount");
                double total = result.getDouble("ReceiptTotal");
                ReceiptModel receipt = new ReceiptModel(rid, created, customerId, employeeId, amount, total);

                return receipt;

            }

        } catch (SQLException ex) {
            System.out.println("Error: Unable to select receipt id" + id + "!!!" + ex.getMessage());
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM Receipt WHERE id=? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error: Unable to delete recipt id" + id + "!!!");
        }

        db.close();
        return row;
    }

    @Override
    public int update(ReceiptModel oblect) {
//        Connection conn = null;
//        String dbPath = "./db/store.db";
//        Database db = Database.getInstance();
//        conn = db.getConnection();
//        int row = 0;
//        try {
//            String sql = "UPDATE product SET name=? ,price=? WHERE id=? ";
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setString(1, oblect.getName());
//            stmt.setDouble(2, oblect.getPrice());
//            stmt.setInt(3, oblect.getId());
//            row = stmt.executeUpdate();
//        } catch (SQLException ex) {
//            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        db.close();
        return 0;
    }

}
